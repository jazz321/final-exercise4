from django.contrib.auth import logout, authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from .forms import RecordForm
from .models import RecordBook


def register(request):
	if request.method == 'POST':
		reg_form = UserCreationForm(request.POST)
		if reg_form.is_valid():
			reg_form.save()
			username = reg_form.cleaned_data['username']
			password = reg_form.cleaned_data['password1']
			user = authenticate(request, username=username, password=password)
			auth_login(request, user)
			return redirect('record_list')
	else:
		reg_form = UserCreationForm()
	return render(request, 'registration/register.html', {'reg_form': reg_form,})

def log_out(request):
	logout(request)
	return redirect('login')
	
def addrecord(request):
	if request.user.is_authenticated:
		if request.method == "POST":
			record_form = RecordForm(request.POST)
			if record_form.is_valid():
				record_with_user = record_form.save(commit=False)
				record_with_user.userlog = request.user
				record_with_user.save()
				return redirect('record_list')
		else:
			record_form = RecordForm()
	else:
		return redirect('login')
	return render(request, 'recordbook/recordpage.html', {'record_form': record_form,})

def update_record(request, pk):
	if request.user.is_authenticated:
		a_record = get_object_or_404(RecordBook, pk=pk)
		if request.method == "POST":
			record_form = RecordForm(request.POST, instance=a_record)
			if record_form.is_valid():
				record_form.save()
				return redirect('record_list')
		else:
			record_form = RecordForm(instance=a_record)
	else:
		return redirect('login')
	return render(request, 'recordbook/recordpage.html', {'record_form': record_form,})

def record_list(request):
	if request.user.is_authenticated:
		records = RecordBook.objects.filter(userlog=request.user)
	else:
		return redirect('login')
	return render(request, 'recordbook/record_list.html', {'records': records,})

# Delete Record
def delrecord(request, pk):
	if request.user.is_authenticated:
		a_record = get_object_or_404(RecordBook, pk=pk)
	else:
		return redirect('login')
	return render(request, 'recordbook/delrecord.html', {'a_record': a_record})

# Confirming record deletion
def confirm_delete(request, pk):
	a_record = get_object_or_404(RecordBook, pk=pk)
	a_record.delete()
	return redirect('record_list')
